package INSW.DOSP.controller;


import INSW.DOSP.dto.ReturnDto;
import INSW.DOSP.service.ResultService;
import INSW.DOSP.service.TblUserService;
import io.swagger.annotations.ApiOperation;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("NLE/dosp")
@CrossOrigin
public class TblUserController extends BaseController {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ResultService resultService;
    @Autowired
    TblUserService tblUserService;


    @PostMapping(value = "/getUser", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("ambildata")
    ResponseEntity get(@RequestBody ReturnDto request) throws Exception {
        try {
            return ok(resultService.get(request));
        } catch (Exception e) {
            e.getMessage();
            return this.badRequest(e.getMessage());
        }
    }

    @GetMapping(value = "/checkAuth", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("cek auth")
    public String cek() {

        return "valid";
            }
    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("ambil data user")
    ResponseEntity getAll() {
        try {
            return ResponseEntity.ok(tblUserService.getAll());
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return badRequest(e.getMessage());
        }
    }
    @GetMapping(value = "/getById",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("ambil berdasarkan id")
    ResponseEntity getId(@RequestParam String email){
        try {
            return ResponseEntity.ok(tblUserService.getById(email));
        }catch (Exception e){
            logger.debug(e.getMessage());
            return badRequest(e.getMessage());
        }
    }
}




