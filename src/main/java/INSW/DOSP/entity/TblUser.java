package INSW.DOSP.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tbluser")
public class TblUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userid")
    private Double userID;

    @Column(name = "jabatan")
    private String jabatan;
    @Column(name = "nosik")
    private String nosik;
    @Column(name = "noktp")
    private String noKtp;
    @Column(name = "alamat")
    private String alamat;
    @Column(name = "telepon")
    private String telepon;
    @Column(name = "fax")
    private String fax;
    @Column(name = "kodepos")
    private String kodePos;
    @Column(name = "kota")
    private String kota;
    @Column(name = "hp")
    private String hp;
    @Column(name = "email")
    private String email;
    @Column(name = "userlogin")
    private String userLogin;
    @Column(name = "password")
    private String Password;
    @Column(name = "fullname")
    private String fullName;
    @Column(name = "orgid")
    private Double orgId;
    @Column(name = "status")
    private String status;
    @Column(name = "last_update")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @Column(name = "expiry")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;
    @Column(name = "blokir_date")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date blokirDate;
    @Column(name = "idblock")
    private Double idBlock;
    @Column(name = "nip")
    private String nip;
    @Column(name = "ap")
    private String ap;
    @Column(name = "kdkpbc")
    private String kdkpbc;
    @Column(name = "fl_tunjuk")
    private String flTunjuk;
    @Column(name = "flag_login")
    private String flagLogin;
    @Column(name = "fl_tracking")
    private String flTracking;
    @Column(name = "userid_parent")
    private Double userIdParent;
    @Column(name = "last_login")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Column(name = "rec_by")
    private String recordBy;
    @Column(name = "rec_date")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date recordDate;
    @Column(name = "upd_by")
    private String updateBy;
    @Column(name = "upd_date")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "create_dt")
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
}
































