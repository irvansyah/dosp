package INSW.DOSP.config;


import INSW.DOSP.dto.CommonRs;
import INSW.DOSP.service.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yevdo.jwildcard.JWildcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor implements HandlerInterceptor {
    
    @Autowired
    protected JwtService jwtService;
    protected String[] notSecured = {"/NLE/dosp/getUser",
                                     "/api/NLE/dosp/parameter"
                                     };

    
    protected ObjectMapper objectMapper = new ObjectMapper();
            
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        boolean result = false;
        String errMsg = "Not authorized";
        
        String auth = request.getHeader("Authorization");
        String path = request.getRequestURI();
        
        if (auth == null)
            auth = "";
        
        if (auth.startsWith("Bearer "))
            auth = auth.substring(7);
        
        if (auth.length() > 0) {
        try {
            if (!jwtService.isTokenExpired(auth)) {
                
                System.out.println("==>" + jwtService.getUsernameFromToken(auth));
                request.setAttribute("userId", jwtService.getUsernameFromToken(auth));
                result = true;
            }
            else
                errMsg = "Token has Timeout";
        }catch (Exception e) {
            errMsg = "TokenID is invalid";
        }
        }
        else {
            //System.out.println("prehandle : " + path);
            
            if (JWildcard.matches("/NLE*", path)) {
                
                for (String patt : notSecured) {

                    if (JWildcard.matches(patt, path)) {

                        result = true;
                        break;
                    }
                }
            }
            else
                result = true;
        }
        
        if (!result) {
            
            response.setStatus(401);
            response.setContentType("application/json");
            response.getWriter().write(objectMapper
                                        .writerWithDefaultPrettyPrinter()
                                        .writeValueAsString(new CommonRs(HttpStatus.UNAUTHORIZED.value(), errMsg)));
        }
        
        return result;
    }
}
