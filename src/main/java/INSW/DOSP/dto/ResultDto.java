package INSW.DOSP.dto;

import lombok.Data;

@Data
public class ResultDto {

    private String fullName;
    private String userName;
    private String namaPerusahaan;
    private String npwpPerusahaan;
    private String userRole;

}
