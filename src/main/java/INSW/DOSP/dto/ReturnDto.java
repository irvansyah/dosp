package INSW.DOSP.dto;

import lombok.Data;

@Data
public class ReturnDto {
    private String userLogin;
    private String password;
}
