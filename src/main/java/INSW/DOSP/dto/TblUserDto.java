package INSW.DOSP.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
public class TblUserDto {


//    private String noKtp;
    private Double userID;
    private String alamat;
    private String telepon;
    private String fax;
    private String kodePos;
    private String kota;
    private String hp;
    private String email;
//    private String userLogin;
//    private String Password;
    private String fullName;
    private String jabatan;
    private String nosik;

//    private Double orgId;
//    private String status;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date lastUpdate;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date expiry;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date blokirDate;
//    private Double idBlock;
//    private String nip;
//    private String ap;
//    private String kdkpbc;
//    private String flTunjuk;
//    private String flagLogin;
//    private String flTracking;
//    private Double userIdParent;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date lastLogin;
//    private String recordBy;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date recordDate;
//    private String updateBy;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date updateDate;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss",timezone = "Asia/Jakarta")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date createDate;
}
