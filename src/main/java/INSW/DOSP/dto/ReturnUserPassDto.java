package INSW.DOSP.dto;

import lombok.Data;

@Data
public class ReturnUserPassDto {
    private String token;
    private ResultDto userData;

}
