package INSW.DOSP.service;



import INSW.DOSP.dto.ResultDto;
import INSW.DOSP.dto.ReturnDto;
import INSW.DOSP.dto.ReturnUserPassDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ResultService {

    @Autowired
    EntityManager entityManager;
    @Autowired
    JwtService jwtService;

    private String userLog;

    public ReturnUserPassDto get(ReturnDto request) throws Exception {
        ResultDto resultDto = new ResultDto();
        ReturnUserPassDto returnUserPassDto = new ReturnUserPassDto();

            Query q = entityManager.createNativeQuery("select fullname, userlogin, t2.nmorg as namaperusahaan,  t2.noidorg as npwpperusahaan, t4.namagroup as userrole\n" +
                    "from tbluser t \n" +
                    "inner join tblorganization t2  on t.orgid = t2.orgid \n" +
                    "inner join tblgroupuser t3 on t3.userid = t.userid \n" +
                    "inner join tblgroup t4 on t4.idgroup = t3.idgroup \n" +
                    "where t.userlogin = :userLogin and password = md5(:password)");
            q.setParameter("userLogin", request.getUserLogin());
            q.setParameter("password", request.getPassword());


            List<Object[]> objectsHolder = q.getResultList();


            for (Object[] data : objectsHolder) {
                resultDto.setFullName(data[0].toString());
                resultDto.setUserName(data[1].toString());

                userLog = data[1].toString();
                resultDto.setNamaPerusahaan(data[2].toString());
                resultDto.setNpwpPerusahaan(data[3].toString());
                resultDto.setUserRole(data[4].toString());
//            resultDto.setToken());
//            System.out.println();
//            System.out.println(data[2]);
//            System.out.println(data[3]);
//            System.out.println(data[4]);

            }
            returnUserPassDto.setToken(jwtService.generateToken(userLog));
            returnUserPassDto.setUserData(resultDto);
            return returnUserPassDto;
//        return resultDto;


    }
   }

