package INSW.DOSP.service;

import INSW.DOSP.dto.TblUserDto;
import INSW.DOSP.entity.TblUser;
import INSW.DOSP.repository.TblUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TblUserService {


    @Autowired
    TblUserRepository tblUserRepository;
    @Autowired
    EntityManager entityManager;

    public List<TblUserDto> getAll(){
        List<TblUser> tblUsers = tblUserRepository.findAll();
        List<TblUserDto> userDtos = new ArrayList<>();
        for (TblUser tblUser : tblUsers){
            TblUserDto tblUserDto = new TblUserDto ();
            BeanUtils.copyProperties(tblUser,tblUserDto);
            userDtos.add(tblUserDto);
        }
        return userDtos;
    }

    public TblUserDto getById(String email){
        Optional<TblUser> tblUser = tblUserRepository.findById(email);
        List<TblUserDto> tblUserDtos =new ArrayList<>();
        if (tblUser.isPresent()){
            TblUserDto tblUserDto = new TblUserDto();
            tblUserDto.getEmail();
            BeanUtils.copyProperties(tblUser,tblUserDto);
            tblUserDtos.add(tblUserDto);
        }
        return null;
    }
}


