package INSW.DOSP.repository;

import INSW.DOSP.entity.TblUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TblUserRepository extends CrudRepository<TblUser,String> {
    @Query(value = "select * from tbluser",nativeQuery = true)
    List<TblUser> findAll();
    @Query(value = "select * from tbluser t where t.email",nativeQuery = true)
    List<TblUser> findById(String userLogin,String password);
}
